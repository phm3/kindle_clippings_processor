#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, codecs, datetime, time, re, pprint
from collections import defaultdict
from os.path import expanduser

if len(sys.argv) < 2:

    filename = 'My Clippings.txt'
else:
    filename = sys.argv[1]

storage_place = raw_input("Save to default location [1] or Dropbox [2]: ")
home = expanduser("~")

if storage_place == "1":

    outdir = home + "/"
elif storage_place == "2":

    outdir = home + "/Dropbox/"
else:

    outdir = home + "/"

print "Selected: ", outdir


def process_clipping(clipping):

    # every clipping has a title, content and a date
    clipping_lines = clipping.split('\r\n')
    if not len(clipping_lines) >= 4:
        return ('', '')
    title = clipping_lines[0].split('(')[0]
    #author = clipping_lines[0].split('()')[0]
    author = re.search('\((.*?)\)', clipping_lines[0]).group(1)
    item_name = title.rstrip() + "_:_" + author.rstrip()
    #sys.exit()
    content = "\r\n".join(clipping_lines[3:])

    return item_name, content


def process_file():

    fh = codecs.open(filename, 'r', 'utf-8')
    text = fh.read()
    clippings = text.split('\r\n==========\r\n')

    ebooks = defaultdict(list)
    for c in clippings:
        item_name, content = process_clipping(c)
        if item_name and content:
            ebooks[item_name].append(content)
        #define time to export the files
        now = datetime.datetime.fromtimestamp(time.time()).strftime(
            '%Y-%m-%d_%H:%M:%S')

        os.chdir(outdir)

    if not os.path.isdir('KindleClippings_' + str(now)):

        os.mkdir('KindleClippings_' + str(now))

    for ebook in ebooks.keys():

        ebook = ebook.replace('/', '')
        ebook = ebook.replace('\\', '')
        fh = codecs.open('KindleClippings_' + str(now) + '/' + ebook + '.txt',
                         'w', 'utf-8')
        for content in ebooks[ebook]:
            fh.write(content + '\n\n')
        fh.close()


if __name__ == '__main__':

    process_file()
