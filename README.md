klipper based on kindle_clippings_processor
==========================

Takes as input the "My Clippings.txt" that stores the annotations and markings from the Kindle. and outputs one file per Ebook with all annotations from that book.

Added timestamp on the output folder creation, also, if Dropbox installed, the output can be saved to the Dropbox folder.

Usage: python kindle.py (execute the command in the directory with the file "My Clippings.txt" that you can copy from your Kindle). Or use: kindle.py /path/to/clippings 
